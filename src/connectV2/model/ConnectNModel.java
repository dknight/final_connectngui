package connectV2.model;

import connectV2.view.PlayAgain;

import connectV2.view.BoardFrame;

public class ConnectNModel {

	public ConnectNModel() {
		//constructor
	}
	
	public int showPlayAgain(String result) {
		//trigger play again dialog
		return PlayAgain.showPlayAgain(result);
	}
}
